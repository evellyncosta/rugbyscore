package com.example.android.rugbyscore;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int pontuacaoA = 0;
    int pontuacaoB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.scoreA);
        scoreView.setText(String.valueOf(score));
    }

    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.scoreB);
        scoreView.setText(String.valueOf(score));

    }

    public void pontuacaoTryA(View v){
        pontuacaoA = pontuacaoA + 5;
        displayForTeamA(pontuacaoA);
    }

    public void pontuacaoConversaoA(View v){
        pontuacaoA = pontuacaoA + 2;
        displayForTeamA(pontuacaoA);
    }

    public void pontuacaoDropA(View v){
        pontuacaoA = pontuacaoA + 3;
        displayForTeamA(pontuacaoA);
    }

    public void pontuacaoPenalidadeA(View v){
        pontuacaoB = pontuacaoB + 3;
        displayForTeamB(pontuacaoB);
    }

    public void pontuacaoTryB(View v){
        pontuacaoB = pontuacaoB + 5;
        displayForTeamB(pontuacaoB);
    }

    public void pontuacaoConversaoB(View v){
        pontuacaoB = pontuacaoB + 2;
        displayForTeamB(pontuacaoB);
    }

    public void pontuacaoDropB(View v){
        pontuacaoB = pontuacaoB + 3;
        displayForTeamB(pontuacaoB);
    }

    public void pontuacaoPenalidadeB(View v){
        pontuacaoA = pontuacaoA + 3;
        displayForTeamA(pontuacaoA);
    }

    public void reset(View v){
        pontuacaoA = 0;
        pontuacaoB = 0;
        displayForTeamA(pontuacaoA);
        displayForTeamB(pontuacaoB);
    }
}